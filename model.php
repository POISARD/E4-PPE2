<?php
	// Retourne toutes les conferences
	function getLesConferences(){
		// Instancie l'objet de connexion PDO
		$bdd = new PDO('mysql:host=localhost;dbname=formaweb;charset=utf8', 'formaweb_user', 'secret');
		// Envoi de la requete SQL
		$lesConferences = $bdd->query('select * from Conference order by idConf desc');
		
		return $lesConferences;
	}