# Application Silex de gestion et consultation d'articles

Installation et déploiement ...

## Installation de la base de données

Pour installer la base de données, il vous faudra éxecuter les 3 scripts suivant dans cet ordre :

 1. Database.sql
 2. Structure.sql
 3. Content.sql
 
Ces fichiers vont créer la base de données et lui donner les droits sur un compte de gestion, créer une table Conference puis y insérer 3 exemples.