<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link href="formaweb.css" rel="stylesheet" />
		<title>Gestion ds Formations</title>
	</head>
	<body>
		<header>
			<h1>Les formations</h1>.
		</header>
		<?php foreach ($lesConferences as $laConference): ?> 
		<conference>
			<h2><?php echo $laConference['intituleConf'] ?></h2>
			<p><?php echo $laConference['descriptionConf'] ?></p>
		</conference>
		<?php endforeach ?>
		<footer class="footer">
			<a href="./">Formations</a> est un petit site web construit comme exemple de développement moderne en PHP.
		</footer>
	</body>
</html>